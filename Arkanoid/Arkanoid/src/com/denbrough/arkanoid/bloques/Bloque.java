package com.denbrough.arkanoid.bloques;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

public interface Bloque {

	void draw(ShapeType renderer);
	
	void update(float delta);
	
	boolean collides(Vector2 pos);
	
	int explodes();
	
}
