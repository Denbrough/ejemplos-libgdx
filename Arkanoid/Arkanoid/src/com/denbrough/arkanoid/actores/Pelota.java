package com.denbrough.arkanoid.actores;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

public class Pelota {

	private Vector2 pos;
	private Color color;
	public static final int RAD = 8;
	private final Vector2 speed = new Vector2(6, 6);

	public Pelota(Vector2 pos) {
		this(pos, Color.WHITE);
	}

	public Pelota(Vector2 pos, Color color) {
		super();
		this.pos = pos;
		this.color = color;
	}

	public void onEvent() {
		// TODO Auto-generated method stub

	}

	public void draw(ShapeRenderer renderer) {
		renderer.begin(ShapeType.FilledCircle);
		renderer.setColor(this.color);
		renderer.filledCircle(pos.x, pos.y, Pelota.RAD);
		renderer.end();

	}

	public Vector2 update(float timeDelta, Vector2 palaPos) {
		int auxX = (int) (this.speed.x + this.pos.x);
		int auxY = (int) (this.speed.y + this.pos.y);

		if (auxX < Gdx.graphics.getWidth() && auxX > 0)
			this.pos.x = auxX;
		else
			this.speed.x = this.speed.x * (-1);

		if (auxY < Gdx.graphics.getHeight())
			this.pos.y += this.speed.y;
		else
			this.speed.y = this.speed.y * (-1);

		if (auxY < palaPos.y + Pala.ALTO + Pelota.RAD && auxX > palaPos.x
				&& auxX < palaPos.x + Pala.ANCHO)
			this.speed.y *= -1;
		
		return pos;
	}

	public void setPos(Vector2 pos) {
		this.pos = pos;
	}

	public void setPos(int x, int y) {
		this.pos.x = x;
		this.pos.y = y;
	}

}
