package com.denbrough.arkanoid.actores;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

public interface ArkanoidActor {

	/**
	 * Detecto eventos, ya sea del teclado o del raton
	 * y actua en consecuencia
	 */
	public abstract void onEvent();

	/**
	 * Dibuja al actor en escena
	 */
	public abstract void draw(ShapeRenderer shape);

	
	/**
	 * Actualiza la posicion del actor o cualquier otro valor de este
	 * @param timeDelta
	 */
	public abstract Vector2 update(float timeDelta);

}