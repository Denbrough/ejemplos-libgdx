package com.denbrough.arkanoid.actores;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

public class Pala {

	private Vector2 pos;
	private Color color;
	public static final int ANCHO = 100;
	public static final int ALTO = 16;
	private final int speed = 6;

	public Pala(Vector2 pos) {
		super();
		this.pos = pos;
		this.color = Color.RED;
	}

	public void onEvent() {
		// TODO Auto-generated method stub

	}

	public void update(float timeDelta) {
		// TODO Auto-generated method stub

	}

	public void draw(ShapeRenderer renderer) {
		renderer.begin(ShapeType.FilledRectangle);
		renderer.setColor(this.color);
		renderer.filledRect(pos.x, pos.y, Pala.ANCHO, Pala.ALTO);
		renderer.end();

	}

	public Vector2 getPos() {
		return pos;
	}

	public void setPos(int x) {
		this.pos.x += x * speed;
	}

}
