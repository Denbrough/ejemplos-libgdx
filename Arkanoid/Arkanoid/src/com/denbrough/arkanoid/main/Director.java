package com.denbrough.arkanoid.main;

import com.badlogic.gdx.Game;
import com.denbrough.arkanoid.scenes.LevelScene;

public class Director extends Game {

	@Override
	public void create() {
		this.setScreen(new LevelScene());
		
	}
	
}
