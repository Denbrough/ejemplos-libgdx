package com.denbrough.arkanoid.scenes;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.denbrough.arkanoid.actores.Pala;
import com.denbrough.arkanoid.actores.Pelota;
import com.denbrough.arkanoid.bloques.Bloque;

public class LevelScene implements Screen {

	private Pala pala;
	private Pelota pelota;

	private ShapeRenderer renderer;

	private boolean game = false;
	
	private ArrayList<ArrayList<Bloque>> nivel;

	public LevelScene() {
		super();

		this.pala = new Pala(new Vector2((Gdx.graphics.getWidth() / 2)
				- (Pala.ANCHO / 2), (int) Gdx.graphics.getHeight() * 2 / 100));

		this.pelota = new Pelota(new Vector2(50, 50));

		this.nivel = new ArrayList<ArrayList<Bloque>>();
		
		this.renderer = new ShapeRenderer();
	}

	@Override
	public void render(float delta) {
		Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);

		if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
			pala.setPos(-1);
		else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
			pala.setPos(+1);
		
		if (Gdx.input.isKeyPressed(Input.Keys.SPACE))
			this.game = true;

		pala.draw(renderer);
		this.pelota.draw(renderer);

		if (this.game == true)
			this.pelota.update(delta, this.pala.getPos());
		else
			this.pelota.setPos((int)this.pala.getPos().x + Pala.ANCHO / 2,
					(int)this.pala.getPos().y + Pala.ALTO + Pelota.RAD);

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
